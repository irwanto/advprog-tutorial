package id.ac.ui.cs.advprog.tutorial3.composite;

import id.ac.ui.cs.advprog.tutorial3.composite.higherups.Ceo;
import id.ac.ui.cs.advprog.tutorial3.composite.higherups.Cto;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.BackendProgrammer;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.FrontendProgrammer;

public class CompositeMain {
    public static void main(String[] args) {
        Company ApCompany = new Company();

        Employees CEO = new Ceo("Siio", 500000);
        Employees CTO = new Cto("Sitio", 250000);
        Employees backEndProgrammer1 = new BackendProgrammer("Beken", 100000);
        Employees backEndProgrammer2 = new BackendProgrammer("Beken II", 100000);
        Employees frontEndProgrammer1 = new FrontendProgrammer("Pronen", 100000);

        ApCompany.addEmployee(CEO);
        ApCompany.addEmployee(CTO);
        ApCompany.addEmployee(backEndProgrammer1);
        ApCompany.addEmployee(backEndProgrammer2);
        ApCompany.addEmployee(frontEndProgrammer1);

        System.out.println("List of ApCompany's employees salaries");
        for (Employees e: ApCompany.getAllEmployees()) {
            System.out.println("- " + e.getName() + " (" + e.getRole() + ") " + e.getSalary());
        }
        System.out.println("Total salary is " + ApCompany.getNetSalaries());
    }
}
