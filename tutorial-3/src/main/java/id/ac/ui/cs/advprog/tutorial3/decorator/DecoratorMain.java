package id.ac.ui.cs.advprog.tutorial3.decorator;

import id.ac.ui.cs.advprog.tutorial3.decorator.bread.CrustySandwich;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.Cheese;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.ChickenMeat;

public class DecoratorMain {
    public static void main(String[] args) {
        Food myOrder = new Cheese(new ChickenMeat(new CrustySandwich()));
        System.out.println("You ordered " + myOrder.getDescription());
    }
}
